import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'flightDuration'
})
export class FlightDurationPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): string {
    const totalMinutes:number= value % 60 ;
    const totalHours:number= parseInt((value / 60).toString());
    return `${totalHours}:${totalMinutes}`;
  }

}
