import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class WorkersService {
  constructor(private httpService: HttpClient) {

  }
  private selectedWorker$: Subject<any> = new Subject();
  public setSelectedWorker(selectedWorker: any) {
    this.selectedWorker$.next(selectedWorker);
  }
  public onSelectedWorkerChange(callback) {
    this.selectedWorker$.subscribe(worker => callback(worker));
  }

  public getAllWorkers() {
    const url = 'https://interview-mock.herokuapp.com/api/workers/';
    return this.httpService.get(url);
  }
  getWorkerFlights(id: any) {
    const url = `https://interview-mock.herokuapp.com/api/workers/${id}`;
    return this.httpService.get(url);
  }
}
