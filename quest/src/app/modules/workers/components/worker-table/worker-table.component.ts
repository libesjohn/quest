import { Component, OnInit } from '@angular/core';
import { WorkersService } from '../../services/workers.service';

const MINUTE = 60 * 1000;
@Component({
  selector: 'app-worker-table',
  templateUrl: './worker-table.component.html',
  styleUrls: ['./worker-table.component.scss']
})
export class WorkerTableComponent implements OnInit {
  public tableColumns = [{ key: 'num', title: 'Flight #' }, { key: 'from', title: 'Origin' }, { key: 'from_date', title: 'Origin Date' }, { key: 'to', title: 'Destination' }, { key: 'to_date', title: 'Destination Date' }];
  public worker: any = null;
  public flights: any;
  public selectedFlight = null;
  private setTimeoutId;
  constructor(private workersService: WorkersService) { }

  ngOnInit(): void {
    this.workersService.onSelectedWorkerChange(worker => this.getWorkerFlights(worker))
  }
  getWorkerFlights(worker) {
    this.cancelAutoUpdate();
    this.worker = worker;
    const { id } = worker;
    id && this.workersService.getWorkerFlights(id).subscribe(resp => {
      this.flights = resp || [];
      this.flights.length > 0 && this.onSelectFlight(this.flights[0]);
    });
    setTimeout(() => this.getWorkerFlights(this.worker), MINUTE);
  }
  onSelectFlight(flight) {
    this.selectedFlight = flight;
  }
  ngOnDestroy() {
    this.cancelAutoUpdate();
  }
  cancelAutoUpdate() {
    this.setTimeoutId && clearTimeout(this.setTimeoutId);
  }
}
