import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-flight-info',
  templateUrl: './flight-info.component.html',
  styleUrls: ['./flight-info.component.scss']
})
export class FlightInfoComponent implements OnInit {
  @Input() flight: any = null;
  info: any = [{ key: 'plane', title: 'Plane Number' }, { key: 'duration', title: 'Duration', format: 'date', formatParam: 'HHmm' }, { key: 'from_gate', title: 'Origin gate' }, { key: 'to_gate', title: 'Destination gate' }];
  constructor() { }

  ngOnInit(): void {
    console.log(Object.keys(this.flight));
  }
}
