import { Component, Input, OnInit } from '@angular/core';
import { WorkersService } from '../../services/workers.service';

@Component({
  selector: 'app-workers-list',
  templateUrl: './workers-list.component.html',
  styleUrls: ['./workers-list.component.scss']
})
export class WorkersListComponent implements OnInit {
  @Input() public initAllWorkers: boolean = false;
  @Input() public allWorkersList: any = [];
  public selectedWorker: any = null;
  constructor(private workersService: WorkersService) { }

  ngOnInit(): void {
    this.initAllWorkers && this.getAllWorkers();
  }
  private getAllWorkers() {
    this.workersService.getAllWorkers().subscribe(resp => this.allWorkersList = resp || []);
  }
  public selectWorker(worker) {
    this.selectedWorker = worker;
    this.workersService.setSelectedWorker(this.selectedWorker);
  }
}
