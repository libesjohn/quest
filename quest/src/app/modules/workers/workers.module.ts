import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WorkersListComponent } from './components/workers-list/workers-list.component';
import { WorkerTableComponent } from './components/worker-table/worker-table.component';
import { WorkersService } from './services/workers.service';
import { FlightInfoComponent } from './components/flight-info/flight-info.component';
import { FlightDurationPipe } from './flight-duration.pipe';


const components = [WorkerTableComponent, FlightInfoComponent, WorkersListComponent];
@NgModule({
  declarations: [...components,FlightDurationPipe],
  imports: [
    CommonModule
  ],
  providers: [WorkersService],
  exports: components
})
export class WorkersModule { }
